const anotherFunction = () => {
    // Con var siempre nos da el último elemento
    // Con let tenemos el valor actual
    for (let i = 0; i < 10 ; i++) {
        setTimeout( () => {
            console.log(i);
        } , 1000)      
        
    }
};

anotherFunction();